#### {{footer-col-1-header[Footer column 1 header] Find Acceptable Ads on}}

- [ {{footer-abp-desktop[Text of the link in the footer] Adblock Plus for desktop browsers }} ](https://adblockplus.org/)

- [ {{ footer-abb[Text of the link in the footer] Adblock Browser for Android and iOS }} ](https://adblockbrowser.org/)

- [ {{ footer-abp-safari[Text of the link in the footer] Adblock Plus for Safari on iOS }} ](https://itunes.apple.com/app/adblock-plus-abp/id1028871868)

- [ {{ footer-adblock[Text of the link in the footer] ​AdBlock products }} ](https://getadblock.com/)

- [ {{ footer-premium-adblock[Text of the link in the footer] ​AdBlock Premium products }} ](https://getadblock.com/)

- [ {{ footer-ublock[Text of the link in the footer] ​uBlock products }} ](https://www.ublock.org/)

- [ {{ footer-crystal[Text of the link in the footer] Crystal products }} ](https://crystalapp.co/)
