## {{ join-committee-heading[heading] The Acceptable Ads Committee is at the forefront of online change }}

{{ join-committee-1 We want to empower users, but also find balance and sustainability for an online world where publishers, advertisers, users and nonprofit organizations all have a say in creating the standards that will make the web a better place.}}
{: .fg-gray }

{{ join-committee-2 Fill out the form below and join us in shaping the future of the web. }}
{: .fg-gray }
