# {{ aac-heading[heading] The Acceptable Ads Committee }}

{{ aac-1 The independent Acceptable Ads Committee (AAC), established in 2017, creates exceptional ad standards. This improves user experience while delivering real value to content publishers and online advertisers. }}

{{ aac-2 The AAC determines the criteria that define which ads are acceptable. It also governs the Acceptable Ads Standard. }}

[{{ aac-apply-link-1[button text] Join the Acceptable Ads Committee }}](#apply)
