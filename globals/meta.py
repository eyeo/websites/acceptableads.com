meta = {
    'og_image': 'https://acceptableads.com/img/png2x/social-media-promo.png',
    'twitter_card': 'summary-large-image',
    'twitter_site': '@eyeo',
    'twitter_creator': '@eyeo',
    'twitter_image': 'https://acceptableads.com/img/png2x/social-media-promo.png',
    'twitter_image_alt': 'Acceptable Ads promotional image'
}
