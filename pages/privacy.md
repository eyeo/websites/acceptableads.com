title=Privacy Policy
description=This Privacy Policy describes how we use, process and protect information that we collect from you online via the Acceptable Ads website.

<script src="/js/address-masking.js"></script>

<div class="clear-navbar section-container" markdown="1">

# {{ privacy-heading[Page Heading] Privacy Policy (AcceptableAds.com) }}

<nav>
  <ul class="plain-list">
    <li><a href="#short">Privacy Notice (short version)</a></li>
    <li><a href="#long">Privacy Notice (long version)</a></li>
  </ul>
</nav>

## {{ privacy-policy-short-heading[heading] Privacy Notice (short version) }} {: #short }

{{ privacy-policy-short-paragraph Your protection and data confidentiality is of utmost importance to us ("**eyeo**", "**we**", "**our**"). The following privacy notice shall provide you with a general overview about the collection, processing and use (hereinafter together referred to as "**processing**") of your personal data on our website AcceptableAds.com (hereinafter referred to as "**website**"). For more information regarding our processing activities, please view our complete [Privacy Policy](#long). }}

### {{ data-processed-heading[heading] What kind of data do we process? }} {: #data-processed }

#### 1. {{ data-processed-item-website-default When using our website, by default: }}

* {{ data-processed-item-1[list item] IP address (stored separately) }}
* {{ data-processed-item-2[list item] Date and time of access }}
* {{ data-processed-item-3[list item] The URL accessed }}
* {{ data-processed-item-4[list item] Browser name/version }}
* {{ data-processed-item-5[list item] URL of previously visited webpage }}
* {{ data-processed-item-6[list item] Amount of data sent }}

#### 2. {{ data-processed-item-newsletter When subscribing to a newsletter }}

##### {{ data-processed-item-newsletter-automatically-heading[heading] Automatically: }}

* {{ data-processed-item-7[list item] Name }}
* {{ data-processed-item-8[list item] Email address }}

##### {{ data-processed-item-newsletter-voluntary-heading[heading] Voluntary: }}

* {{ data-processed-item-9[list item] Organization }}
* {{ data-processed-item-10[list item] Role / title }}

#### 3. {{ data-processed-item-committee When applying for the Acceptable Ads Committee (AAC) }}

##### {{  data-processed-item-committee-automatically-heading[heading] Automatically: }}

* {{ data-processed-item-11[list item] Name }}
* {{ data-processed-item-12[list item] Title }}
* {{ data-processed-item-13[list item] Company or organization's name }}
* {{ data-processed-item-14[list item] Email address }}
* {{ data-processed-item-15[list item] Stakeholder group }}
* {{ data-processed-item-16[list item] Short description of your application motivation }}

##### {{ data-processed-item-committee-voluntary-heading[heading] Voluntary: }}

* {{ data-processed-item-17[list item] Phone number }}
* {{ data-processed-item-18[list item] Additional contact }}

#### 4. {{ data-processed-item-initiative When applying to participate in the Acceptable Ads initiative as a publisher, advertiser, SSP, DSP or other ad tech provider }}

##### {{ data-processed-item-initiative-automatically-heading[heading] Automatically: }}

* {{ data-processed-item-19[list item] First and last name }}
* {{ data-processed-item-20[list item] Email address }}

##### {{ data-processed-item-initiative-voluntary-heading[heading] Voluntary: }}

* {{ data-processed-item-21[list item] Company name }}
* {{ data-processed-item-22[list item] Job title }}
* {{ data-processed-item-23[list item] Company website }}

#### 5. {{ data-processed-item-social-media[heading] On our social media pages }}

* {{ data-processed-item-24[list item] Usage profiles }}

### {{ how-we-collect-heading[heading] How do we collect data? }} {: #how-we-collect-data }

{{ list-of-tools[preface] List of techniques and tools we use for data collection: }}
: {{ tools-item-1[list item] Log files }}
: {{ tools-item-2[list item] Newsletter subscriptions }}
: {{ tools-item-4[list item] Application signup form }}
: {{ tools-item-5[list item] Customer relation management tool }}
: {{ tools-item-6[list item] Data you provide to us with via social media }}
: {{ tools-item-7[list item] In connection with our social media pages:  }}
    : {{ tools-item-8[list item] Cookies  }}

### {{ why-we-process-heading[heading] How and why do we process your data? }} {: #why-we-process-data }

* {{ why-we-process-1 For technical purposes, such as, but not limited to, preventing security attacks, to improve our website and to ensure website security. }}
* {{ why-we-process-2 If requested by you, to inform you about or contact you in connection with our products and / or to send you reports or other information. }}
* {{ why-we-process-4 To analyze aggregated website logs to improve our website. }}
* {{ why-we-process-5 In connection with our social media pages, social networks use the data for market research and advertising purposes. }}

### {{ what-is-legal[heading] What is the legal basis of data processing? }} {: #what-is-legal }

{{ what-is-legal We process your personal data in compliance with the European General Data Protection Regulation ([Regulation (EU) 2016/679](//gdpr-info.eu)) ("**GDPR**"), the applicable EU laws and German national data protection laws. }}

### {{ how-long-heading[heading] How long do we keep data? }} {: #how-long }

* {{ how-long-item-1[list item] Website logs are stored for 30 days. **Note**: “Aggregated usage statistics” and data without any connection to single users may be retained beyond these periods. }}
* {{ how-long-item-2[list item] Acceptable Ads Committee applicant data: }}
* {{ how-long-item-3[list item] If becoming a member or representative, for a maximum period of one (1) month after resignation. }}
* {{ how-long-item-4[list item] If rejected, for a maximum period one (1) month after rejection. }}

### {{ our-values-heading[heading] Our values }} {: #our-values }

{{ our-values-paragraph We collect as little data as possible. If anonymous or pseudonymous use is possible, we will anonymize or pseudonymize your data. }}

### {{ your-rights-heading[heading] What rights do you have? }} {: #your-rights }

{{ your-rights In compliance with the GDPR, and the applicable EU laws and German national data protection laws and to the extent legally permitted, you have the right to: }}
: {{ your-right-item-1[list item] Receive information about your personal data processed by us, how we process your data, and how we obtain such data. }}
: {{ your-right-item-2[list item] Rectify inaccurate personal data and restrict details. }}
: {{ your-right-item-3[list item] Receive all your personal data in a structured, commonly used and machine-readable format, as well as having such data transmitted to another controller. }}
: {{ your-right-item-4[list item] Request erasure of your data, unless such data needs to be retained for legal purposes. }}
: {{ your-right-item-5[list item] Object to the processing of your data. }}
: {{ your-right-item-6[list item] Withdraw your consent at any time, when you have provided us with your consent to the processing of your personal data. }}
: {{ your-right-item-7[list item] Lodge a complaint with the respective supervisory authority. }}

### {{ questions-heading[heading] Questions? }} {: #questions }

{{ questions-paragraph Contact our Data Protection Officer, Dr. Judith Nink, via <a title="email address of Data Protection Officer at eyeo GmbH" data-mask='{"href": "bWFpbHRvOnByaXZhY3lAZXllby5jb20="}'>email</a> or <a title="telephone number of Data Protection Officer at eyeo GmbH">phone</a>. }}

## {{ privacy-policy-long-heading[heading] Privacy Policy (long version) }} {: #long }

### {{ privacy-general-info-heading[heading]  General information about your privacy}}

{{ privacy-general-info The following information applies to the collection, processing and use of personal data in connection with our services, as but not limited to, the Adblock Plus extension, Adblock Browser and on our websites. }}

1. [{{ general-notes General notes }}](#general-notes)
2. [{{ who-is-responsible Who is responsible for data collection and processing? }}](#who-is-responsible)
3. [{{ what-is-personal-data What is personal data? }}](#what-is-personal-data)
4. [{{ what-is-the-purpose What is the purpose of data processing and what is the legal basis? }}](#what-is-purpose)
5. [{{ do-we-disclose Do we disclose any personal data? }}](#disclosure)
6. [{{ what-rights What rights do you have? }}](#your-rights-long)
7. [{{ changes-to-policy Changes to this Privacy Policy }}](#changes-to-policy)
8. [{{ what-kind-of-data What kind of data do we collect and process, and how? }}](#data-collect-process)

### {{ general-notes-heading[heading] General notes }} {: #general-notes}

{{ general-notes-paragraph Your protection and data confidentiality is of utmost importance to us (**"eyeo"**, **"we"**, **"our"**). We take the protection of your personal data very seriously and collect as little data as possible. Nevertheless, collecting data helps our products and websites function correctly, and allows us to communicate with you. Our general Privacy Policy is to avoid collecting more data than necessary. Collected data is anonymized or pseudonymized, if possible, and deleted when no longer needed. This Privacy Policy shall inform you about the collection, processing and use of your personal data. We gather and use personal data firmly within the provisions of the European General Data Protection Regulation ([Regulation (EU) 2016/679](//gdpr-info.eu)) (GDPR), the applicable EU Laws and German national data protection laws. In the following text, we will inform you about the specific data, the scope, and the purpose of the collection and use of personal data by eyeo when using our products and visiting our websites. }}

### {{ who-is-responsible-heading[heading] Who is responsible for data collection and processing (contacts)? }} {: #who-is-responsible }

{{ who-is-responsible-paragraph-1 The legal person responsible for the collection, processing and / or use of personal data in connection with our websites and products ("**Controller**") is: }}

#### {{ controller-heading[heading] Controller }} {: #controller }

<address>
eyeo GmbH<br>
Lichtstr. 25<br>
50823 Cologne<br>
Germany<br>
</address>

#### {{ dpo-heading[heading] Data Protection Officer }} {: #data-protection-officer }

{{ who-is-responsible-paragraph-2 If you have any questions regarding your personal data, please do not hesitate to contact our Data Protection Officer: }}

<address>
  Dr. Judith Nink
  <dl class="plain-list">
    <dt>Phone</dt>
    <dd><a id='privacy-phone' href='tel:+4922165028598'>+49 (0) 221 / 65028 598</a></dd>
    <dt>Email</dt>
    <dd><a id='privacy-email' data-mask='{"href": "bWFpbHRvOnByaXZhY3lAZXllby5jb20=", "textContent": "cHJpdmFjeUBleWVvLmNvbQ=="}'>email</a></dd>
    <dt>Fax</dt>
    <dd><a id='privacy-fax' href='fax:+4922165028599'>+49 (0) 221 / 65028 599</a></dd>
  </dl>
</address>

### {{ what-is-personal-heading[heading] What is personal data? }} {: #what-is-personal-data }

{{ what-is-personal-paragraph The purpose of data protection is to protect personal data. Personal data means any information relating to an identified or identifiable natural person ("**data subject**"). An identifiable natural person is one who can be identified, directly or indirectly, in particular, by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.
This information includes, for example, details such as name, postal address, email address or telephone number, but also nicknames, certificates and information about your interests. }}

### {{ data-collect-process-heading[heading] What kind of data do we collect and process, and how? }} {: #data-collect-process }

#### {{ data-collect-process-logs[heading] Website logs (by default) }} {: #data-collect-process-logs }

{{ data-collect-process-logs-paragraph-1 While using the website, we automatically record website logs thereby collecting the following data for technical and for security reasons, and to provide you with our services: }}
: {{ data-collect-process-logs-item-1[list item] IP address (stored separately) }}
: {{ data-collect-process-logs-item-1[list item] Date and time of access }}
: {{ data-collect-process-logs-item-1[list item] Browser name/version<sup><a href="#rfc1">1</a></sup> }}
: {{ data-collect-process-logs-item-1[list item] URL of previously visited webpage<sup><a href="#rfc2">2</a></sup> }}
: {{ data-collect-process-logs-item-1[list item] Amount of data sent }}

---

1\. {{ data-collect-process-logs-footnote-1 For more information, please refer to [RFC 7231 Section 5.5.3](//tools.ietf.org/html/rfc7231#section-5.5.3){: #rfc1 .no-scroll } }}
{: .small }

2\. {{ data-collect-process-logs-footnote-2 For more information, please refer to [RFC 7231 Section 5.5.2](//tools.ietf.org/html/rfc7231#section-5.5.2){: #rfc2 .no-scroll } }}
{: .small }

{{ data-collect-process-logs-paragraph-2 This data is stored purely for technical reasons and cannot be linked to any individual person. We do not combine the website log data with any other information about you. }}

#### {{ data-collect-process-retention-heading[heading] Data retention }} {: #data-retention }

{{ data-collect-process-retention-paragraph Such website logs are retained for a period of 30 days, after which only the aggregated usage statistics that cannot be connected to a single user remain. Everything else is deleted. }}

#### {{ voluntary-info-heading[heading] Information you give to us on a voluntary basis }} {: #voluntary-info }

{{ voluntary-info-paragraph You can use the website without providing us with any additional personal data. However, if you want to contact us or apply to the Acceptable Ads Committee, we need some further information in order to respond to your requests, questions and criticism, and to evaluate your application to the Acceptable Ads Committee. }}

#### {{ voluntary-info-application-form-heading[heading] Application form }} {: #voluntary-info-application-form }

{{ voluntary-info-application-form-paragraph If you want to apply to the Acceptable Ads Committee, you must provide us with your name, title, company or organization's name, your email address, your stakeholder group and a short description of your motivation for wanting to join the Acceptable Ads Committee. This information is necessary to evaluate your applications and choose qualified members. Optionally, you can provide us with your phone number as an additional contact. For building up the first Acceptable Ads Committee, we will evaluate your application. At a later stage, the Acceptable Ads Committee members will take over the evaluation of applications. Therefore, we will share the applications with the Acceptable Ads Committee members. One of the aims of the Acceptable Ads Committee is transparency. According to the Bylaws, name and company name of applications to join Member Groups, nominations, resignations and terminations must be published on the Acceptable Ads Committee website. In order to fulfill this purpose, we will publish the following information of approved applications on the Acceptable Ads website: name, title, company or organization's name, as well as the respective stakeholder group. }}

#### {{ voluntary-info-data-retention-heading[heading] Data retention }} {: #voluntary-info-data-retention }

{{ Voluntary-info-data-retention-paragraph We keep application data for maximum period of one (1) month after (i) resignation, provided you have joined the Acceptable Ads Committee, or (ii) rejection, provided your application has been rejected. }}

### {{ what-is-purpose-heading[heading] What is the purpose of data processing and what is the legal basis? }} {: #what-is-purpose }

#### {{ purpose-heading[heading] Purpose of data collection and processing }} {: #purpose }

{{ purpose-paragraph-1 In compliance with [Art. 5 (b) GDPR](//gdpr-info.eu/art-5-gdpr/), we collect and process your personal data for specified, explicit and legitimate purposes and do not further process your data in a manner that is incompatible with those purposes. }}

<dl class="plain-list">
  <dt>{{ purpose-paragraph-2 We collect and process your personal data solely for the following purposes: }}</dt>
  <dd>1. {{ purpose-item-1 We collect and process website logs for technical purposes. We mainly collect and process such data to prevent security attacks and are thus able to provide our services to you in a secure and data-efficient manner. }}</dd>
  <dd>2. {{ purpose-item-2 We collect and process your personal data relating to Acceptable Ads Committee applications to evaluate and select applicants for the Acceptable Ads Committee. }}</dd>
</dl>

#### {{ legal-basis-heading[heading] Legal basis of data collection and processing }} {: #legal-basis }

{{ legal-basis-paragraph We collect and process your personal data in compliance with the GDPR and the applicable EU laws and German national data protection laws. }}

#### {{ consent-permission-heading[heading] Collection and processing is based on your consent - [Art. 6 (1) a GDPR](//gdpr-info.eu/art-6-gdpr/), [Art. 4 (11) GDPR](//gdpr-info.eu/art-4-gdpr/) }} {: #consent-permission }

{{ consent-permission-paragraph We will always ask for your consent to collect and process your personal data, unless the collection and processing of your personal data is permitted by statutory laws. Where you have provided us with your consent to the collection and processing of your personal data for the aforementioned specific purposes, you have the right to withdraw your consent at any time. }}

#### {{ contract-permission-heading[heading] Collection and processing is necessary for taking steps prior to enter into a contract - [Art. 6 (1) b GDPR](//gdpr-info.eu/art-6-gdpr/) }} {: #contract-permission }

{{ contract-permission-paragraph The collection and processing of your personal data may be necessary for the performance of a contract to which you may be a party. Prior to entering into such a contract, the collection and processing of your personal data may also be necessary in order to take steps at your request. This applies for the evaluation and selection of applicants for the Acceptable Ads Committee. }}

#### {{ compliance-permission-heading[heading] Collection and processing is necessary for compliance with a legal obligation to which the Controller is subject – [Art. 6 (1) c GDPR](//gdpr-info.eu/art-6-gdpr/) }} {: #compliance-permission }

{{ compliance-permission-paragraph Collection and processing of your personal data may be necessary for compliance with a legal obligation to which we are subject under EU laws or the laws of a EU Member State. }}

#### {{ legitimate-interest-permission-heading[heading] Collection and processing is necessary for the purposes of our legitimate interests - [Art. 6 (1) f GDPR](//gdpr-info.eu/art-6-gdpr/) }} {: #legitimate-interest-permission }

{{ legitimate-interest-permission-paragraph The collection and processing of your personal data may be necessary for the purposes of our legitimate interests. We collect and process website logs for technical reasons, such as, but not limited to, preventing denial of service attacks. Denial of service is typically accomplished by flooding the targeted machine or resource with unnecessary requests in an attempt to overload systems and prevent some or all legitimate requests from being fulfilled. Preventing such overloads of our systems and any security issues by denial of service attacks is in your and our vital interest and therefore we use the website logs. Furthermore, we collect and process such data to ensure that our website is constantly improved and adjusted to the changing requirements for an efficient usability and the technical environment. Ensuring the usability of our website is in your and our vital interest and therefore we use such data. }}

### {{ disclosure-heading[heading] Do we disclose any personal data? }} {: #disclosure }

{{ disclsoure-paragraph-1 We may only transfer your personal data to third parties without informing you separately beforehand in the following exceptional cases as explained below: }}
: {{ disclosure-item-1[list item] If required for legal proceedings/investigations, personal data will be transferred to the criminal investigation authorities and, if appropriate, to injured third parties. We will only do this if there are concrete indications of illegal and/or abusive behavior. We are also legally obliged to give certain public authorities information. These are criminal investigation authorities, public authorities which prosecute administrative offenses entailing fines and the German finance authorities. }}
: {{ disclosure-item-2[list item] As part of the further development of our business it may happen that the structure of eyeo GmbH changes. The legal structure may be adapted, subsidiaries, business units or components may be created, bought or sold. In such transactions, customer information may be shared with the transmitted part of the company. In the event of a transfer of personal information, softgarden will ensure that it is done in accordance with this Privacy Policy and the German data protection laws. }}

{{ will-not-transfer We will not transfer your personal data to third parties as a matter of course without letting you know in advance. We will ask for your prior permission unless the transfer of such data is permitted by GDPR or any other applicable EU laws and German national data protection laws. }}

#### {{ international-data-transfers-heading[heading] International data transfers }}

{{ data-transfers-1 For the following services, we use non-EU/EEA service providers. We have carefully selected these external service providers and review them regularly to ensure that your privacy is preserved. The service providers provide sufficient guarantees to ensure an adequate level of data protection and may only use personal data for the purposes stipulated by us and in accordance with our instructions. We also contractually require service providers to treat your personal data solely in accordance with this Privacy Policy and the European data protection laws: }}

{{ data-transfers-2 We use external service provider tools for email (GSuite). These services are provided by Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA. In order to ensure an adequate level of data protection, we have entered into a data processing agreement including the EU Standard Contractual Clauses (processors) – Commission Decision C(2010)593. You can access a copy of this agreement here. }}

{{ data-transfers-3 In addition, we use the customer relation management tool from Copper CRM, Inc., 301 Howard Street #600, San Francisco, California 94105, USA, and the web integration of Copper’s CRM from Zapier, Inc., 548 Market Street #62411, San Francisco, California 94104, USA, for customer relation management and its web integration. In order to ensure an adequate level of data protection, Copper (certificate can be accessed <a href="https://www.privacyshield.gov/participant?id=a2zt0000000CbBKAA0&status=Active">here</a>) participates in the Privacy Shield. We have entered into a data processing agreement with Zapier, Inc. that includes the EU Standard Contractual Clauses (processors) – Commission Decision C(2010)593. You can access a copy of this agreement <a href="https://zapier.com/help/account/data-management/zapiers-data-processing-addendum">here</a>. }}

### {{ your-rights-long-heading[heading] What rights do you have? }} {: #your-rights-long }

{{ your-rights-long-paragraph In compliance with the GDPR and the applicable EU laws and German national data protection laws and to the extent legally permitted, you have the following rights to protect your personal data collected and processed by us: }}

#### {{ information-acces-rights-heading[heading] Information, access, rectification and restriction rights }} {: #information-acces-rights }

{{ information-access-rights-paragraph Naturally you have the right to receive, upon request, information about the personal data stored by us about you and information about how we collect, process and store your personal data. Where that is the case, you have the right to gain access to such personal data stored by us. You have the right to request from us the rectification of your inaccurate personal data. Taking into account the purposes of collecting and processing your data, you have the right to have incomplete personal data completed. You have the right to request restriction of processing. }}

#### {{ data-portability-right-heading[heading] Right to data portability  }} {: #data-portability-right }

{{ data-portability-right-paragraph You also have the right (1) to receive all personal data concerning you and which you have provided to us, in a structured, commonly used and machine-readable format and (2) to transmit that data to another controller. }}

#### {{ erasure-of-data-heading[heading] Right to erasure of your data }} {: #erasure-of-data }

{{ erasure-of-data-paragraph-1 You have the right to demand from us the erasure of your personal data, where – inter alia – one of the following grounds applies: }}

* {{ erasure-of-data-item-1[list item] If we no longer need your personal data for the aforementioned purposes. }}
* {{ erasure-of-data-item-2[list item] If you withdraw your consent on which the collection and processing is based and where there are no other legal grounds for the collection and processing. }}
* {{ erasure-of-data-item-3[list item] If you object to the collection and processing and there are no overriding legitimate grounds for the collection and processing. }}

{{ erasure-of-data-paragraph-2 Please note, if data needs to be retained for legal purposes pursuant to [Art. 17 (3) GDPR](//gdpr-info.eu/art-17-gdpr/), we will restrict the use of the respective data. }}

#### {{ lodge-a-complaint-heading[heading] Right to lodge a complaint with a supervisory authority }} {: #lodge-a-complaint }

{{ lodge-a-complaint-paragraph You have the right to lodge a complaint with a supervisory authority, in particular in the Member State of your habitual residence, place of work or place of the alleged infringement if you consider that the collection and processing of personal data relating to you infringes the GDPR. }}

#### {{ right-to-object-heading[heading] Right to object to the processing of your data }} {: #right-to-object }

{{ right-to-object-paragraph You have the right to object at any time to the collection processing of your personal data on grounds relating to your particular situation, when collection and processing is based on our legitimate interest ([Art. 6 (1) f GDPR](//gdpr-info.eu/art-6-gdpr/)). }}

#### {{ withdraw-consent-heading[heading] Right to withdraw your consent at any time }} {: #withdraw-consent }

{{ withdraw-consent-paragraph You have the right to withdraw your consent at any time when you have provided us with your consent to the collection and processing of your personal data for one or more specific purposes. }}

#### {{ how-to-exercise-heading[heading] How to exercise your rights }} {: #how-to-exercise }

{{ how-to-exercise-paragraph To exercise your rights, please contact us via <a title="email address of Data Protection Officer at eyeo GmbH" data-mask='{"href": "bWFpbHRvOnByaXZhY3lAZXllby5jb20="}'>email</a> or write to: }}

<address>
eyeo GmbH<br>
Lichtstr. 25<br>
50825 Cologne<br>
Germany<br>
</address>

### {{ changes-to-policy-heading[heading] Changes to this Privacy Policy }} {: #changes-to-policy }

{{ changes-to-policy-paragraph This Privacy Policy may be changed from time to time. The respective current version is available at: [acceptableads.com/privacy](//acceptableads.com/privacy). }}

### What kind of data do we collect and process, and how?

#### Website logs (by default)

{{ data-collected While using the website, website logs are automatically recorded. Thereby, we collect the following data for technical and security reasons, and to provide you with our services: }}
: {{ website-logs-item-1[list item] IP address (stored separately) }}
: {{ website-logs-item-2[list item] Date and time of access }}
: {{ website-logs-item-3[list item] Browser name/version<sup><a href="#rfc1b">1</a></sup> }}
: {{ website-logs-item-4[list item] URL of previously visited webpage<sup><a href="#rfc2b">2</a></sup> }}
: {{ website-logs-item-5[list item] Amount of data sent }}

{{ stored-for-technical-reasons This data is stored purely for technical reasons and cannot be linked to any individual person. We do not combine the website log data with any other information about you. }}

##### Data retention

{{ retention-aggregation-deletion Such website logs are retained for a period of 30 days, after which only the aggregated usage statistics that cannot be connected to a single user remain. Everything else is deleted. }}

---

1\. {{ data-collect-process-logs-footnote-1 For more information, please refer to [RFC 7231 Section 5.5.3](//tools.ietf.org/html/rfc7231#section-5.5.3){: #rfc1b .no-scroll } }}
{: .small }

2\. {{ data-collect-process-logs-footnote-2 For more information, please refer to [RFC 7231 Section 5.5.2](//tools.ietf.org/html/rfc7231#section-5.5.2){: #rfc2b .no-scroll } }}
{: .small }

#### {{ voluntary-basis Information you give us on a voluntary basis }}

{{ website-use You can use the website without providing us with any additional personal data. However, if you want to: receive a newsletter, contact us, apply to the Acceptable Ads Committee, and / or apply as a participant in the Acceptable Ads initiative, we need some further information in order to respond to your: requests, questions or criticism, and /or to evaluate your Acceptable Ads Committee application or to assess whether you are eligible to participate in Acceptable Ads. }}

#### {{ newsletter-subscription Newsletter subscription }}

{{ free-of-charge We provide you with a newsletter service free of charge. We use the newsletter to inform you about new products, updates on our products and to send you general information about eyeo. We need your name and email address in order to send you the newsletter. You can enter your email address at acceptableads.com. We will store and use your email address solely to send you the newsletter. Optionally you can also provide us with the name of your organization and/ role / title. }}

{{ how-to-unsubscribe Each newsletter contains information on how to unsubscribe (right to withdraw your consent) from your subscription at any time with immediate effect. }}

#### {{ committee-application-form Acceptable Ads Committee application form }}

{{ committee-application-details If you want to apply to the Acceptable Ads Committee, you must provide us with your name, title, company or organization's name, your email address, your stakeholder group, and a short description of your motivation as to why you would like to join the Acceptable Ads Committee. This information is necessary to evaluate your application and choose qualified members. Optionally, you can provide us with your phone number as an additional contact. For building up the first Acceptable Ads Committee, we will evaluate your application. At a later stage, the Acceptable Ads Committee members will take over the evaluation of applications. Therefore, we will share the applications with the Acceptable Ads Committee members. }}

{{ committee-aims One of the aims of the Acceptable Ads Committee is transparency. According to the Bylaws, the name and company name of applicants to join Member Groups, nominations, resignations, and terminations must be published on the Acceptable Ads Committee webpage. In order to fulfill this purpose, we will publish the following information of approved applicants on the website: name, title, company or organization's name, and the respective stakeholder group. }}

#### {{ participation-form Acceptable Ads participation application form }}

{{ initiative-application If you want to apply to the Acceptable Ads initiative, you must provide your first name, last name, and email address. This information is necessary to in order to contact you. Optionally, you can provide us with your company name, job title, and company website. We only use this data to contact you and to evaluate your application. }}

#### {{ data-retention Data retention }}

{{ we-keep We keep: }}
: {{ newsletter-contact-data Newsletter contact data for a maximum period of two (2) months after withdrawing your consent. }}
: {{ committee-application-data Acceptable Ads Committee application data for a maximum period of one (1) month after (i) resignation, provided you have joined the Acceptable Ads Committee, or (ii) rejection, provided your application has been rejected. }}
: {{ application-data Application data for participating in Acceptable Ads: }}
    : {{ participation-duration If confirmed, for the duration of your participation in Acceptable Ads, and for a minimum of 10 years after any participation is concluded. }}
    : {{ maximum-duration If not confirmed, for a maximum period of one (1) year after the last communication. }}
    
#### {{ social-media-presence[heading] Our social media presence }}

{{ social-media-presence-1 In order to communicate with you, and to inform you about our activities and offers on social networks, we are active on LinkedIn. Therefore, we still inform you about the data processing processes in connection with our presence on the respective social network as follows. }}

{{ social-media-presence-2 If you follow our respective online presence on one or more of the social networks used by us, please note that your data may be processed outside the European Union / the European Economic Area. However, all the networks we use have agreed to comply with EU data protection standards within the framework of the EU-U.S. Privacy Shield. }}

{{ social-media-presence-3 The social networks we use also process your data regularly for market research and advertising purposes. Based on your usage behavior and interests, the networks may create usage profiles which are used, for example, to place advertisements corresponding to your potential interests within and outside the networks. For these purposes, cookies, which store your usage behavior and interests, as well as possibly also the devices you use, are regularly stored on your computer. }}

{{ social-media-presence-3 For a detailed overview of the respective processing operations and opt-out options, please visit the website of each social network, listed below. For the assertion of your rights and requests for information, we also refer you to the respective social networks, where you can exercise your rights most effectively. This is because the social networks have access to your data and can therefore directly take appropriate measures and provide you with the respective information: }}
: {{ social-media-presence-linkedin **LinkedIn** (LinkedIn Ireland Unlimited Company Wilton Place, Dublin 2, Ireland) – Privacy Policy: [https://www.linkedin.com/legal/privacy-policy](https://www.linkedin.com/legal/privacy-policy), Opt-Out: [https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out](https://www.linkedin.com/psettings/guest-controls/retargeting-opt-out), Privacy Shield: [https://www.privacyshield.gov](https://www.privacyshield.gov) }}

<br>
<time datetime="2020-04">{{date[Date of publication] April 2020 }}</time>

</div>
