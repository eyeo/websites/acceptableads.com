title=Imprint
description=Contact and legal information for eyeo GmbH

<div class="clear-navbar section-container" markdown="1">

# {{imprint-heading[Page Heading] Imprint}}

## {{headquarters-heading[Heading of a section] Headquarters}}

<address markdown="1">
eyeo GmbH<br>
Lichtstrasse 25<br>
50825 Cologne<br>
Germany
</address>

## {{branches-heading[Heading of a section] Branch Office}}

<address markdown="1">
eyeo GmbH<br>
Zimmerstr. 69<br>
10117 Berlin<br>
Germany
</address>

## {{contact-heading[Heading of a section] Contact}}

[info@acceptableads.com](mailto:info@acceptableads.com)<br>
[committee@acceptableads.com](mailto:committee@acceptableads.com)<br>
{{phone[Contact item] **Phone:**}} [+49 (0) 221 / 65028 598](tel:+4922165028598)<br>
{{fax[Contact item] **Fax:**}} [+49 (0) 221 / 65028 599](tel:+4922165028599)

## {{legal-info-heading[Heading of a section] Legal Info}}

**{{legal-info-item[Legal Info item] VAT-ID:}}** DE279292414<br>
**{{legal-info-item2[Legal Info item] District Court Cologne:}}** HRB 73508<br>
{{legal-info-item3[Legal Info item] **Managing Directors:** <fix>Till Faida</fix>, <fix>Felix Dahlke</fix>, <fix>Steffen Kiedel</fix>}}

{{legal-info-paragraph[Legal info paragraph] **Person responsible for content according to Sec <fix>55</fix> para. <fix>2</fix> German Interstate Broadcasting Agreement (RStV):** <fix>Ben Williams</fix>, Cologne Office}}

</div>
