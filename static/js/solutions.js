document.addEventListener("DOMContentLoaded", function() {

  // Hide the default partner type option select is opened
  var partnerField = document.getElementById("partner-type");
  var partnerDefault = document.getElementById("default-partner-type");

  partnerField.addEventListener("focus", function() {
    partnerDefault.style.display = "none";
  });

  partnerField.addEventListener("blur", function() {
    partnerDefault.style.display = "block";
  });

  var form = document.getElementById("join");
  var submitButton = document.getElementById("send");

  function preventValidation(e) {
    e.preventDefault();
  }

  function onSubmitAttempt() {
    // Check if the form is valid. If not, proceed.
    // If reportValidity is not supported, proceed.
    if (!((typeof form.reportValidity == "function") && form.reportValidity()))
      // Check if individual inputs are valid.
      scrollToFirstInvalidInput(form, submitButton, onSubmitAttempt,
        preventValidation);
  }

  // suppress validation notifications, pre-submit/scroll
  [].slice.call(form.elements).forEach(function(field) {
    field.addEventListener("invalid", preventValidation);
  });

  submitButton.addEventListener("click", onSubmitAttempt);

  form.addEventListener("submit", function(event) {

    event.preventDefault();

    submitButton.disabled = true;
    document.getElementById("sending-message").hidden = null;

    var formData = new FormData(form);
    var request = new XMLHttpRequest();
    request.open("POST", "https://hooks.zapier.com/hooks/catch/3616879/qanuve/", true);

    request.addEventListener("readystatechange", function() {
      if (request.readyState == 4) {
        document.getElementById("sending-message").hidden = true;
        if (request.status == 200) {
          document.getElementById("success-message").hidden = null;
          document.getElementById("error-message").hidden = true;
        } else {
          document.getElementById("success-message").hidden = true;
          document.getElementById("error-message").hidden = null;
        }
      }
    });

    request.send(formData);
  });
});
