document.addEventListener("DOMContentLoaded", function() {
  var doc = document.documentElement;
  var body = document.body;
  var mainContent = document.getElementById("content");

  // Mobile Navigation
  var menuBar = document.querySelector(".navbar");
  var logo = menuBar.querySelector(".site-title");
  var menuContainer = menuBar.querySelector(".container");
  var menuNav = menuContainer.querySelector("nav");
  var menuToggle = document.getElementById("burger-icon").innerHTML;
  var menuToggleWrapper = document.createElement("span");
  var contentOverlay = document.createElement("div");
  var HEADER_HEIGHT = logo.offsetHeight;

  contentOverlay.classList.add("mobile-overlay");

  createMobileNavigation();

  toggleMobileNavigation();

  function createMobileNavigation() {
    // create the necessary DOM
    body.classList.add("mobile-navbar-enabled");
    // add toogle button
    menuToggleWrapper.classList.add("mobile-navbar-toggle")

    menuToggleWrapper.innerHTML = menuToggle;

    menuContainer.prepend(menuToggleWrapper);
    // clone the Navigation
    mobileNavigation = menuContainer.cloneNode(true);

    mobileNavigation.classList.add("mobile-navbar");

    body.append(mobileNavigation);
    // add mobile overlay
    mainContent.append(contentOverlay);
  }

  function toggleMobileNavigation() {
    menuToggleWrapper.addEventListener("click", function(ev) {
      body.classList.toggle("open");
    });
    document.querySelector("#content .mobile-overlay").addEventListener("click", function(ev) {
      body.classList.remove("open");
    });
  }
});
