'use strict';

var NAVBAR_OFFSET = 86;
var SCROLL_TICK_LENGTH = 10;
var SCROLL_TIME = 500;

var page = document.scrollingElement || document.documentElement; // IE-all

function smoothScrollTo(destination, duration) {
  if (duration <= 0) return;

  var distance = (destination - page.scrollTop);
  var perTick = ((distance / duration) * SCROLL_TICK_LENGTH);

  setTimeout(function() {
    page.scrollTop = (page.scrollTop + perTick);

    (page.scrollTop != destination) &&
      smoothScrollTo(destination, (duration - SCROLL_TICK_LENGTH));
  }, SCROLL_TICK_LENGTH);
}

[].slice.call(document.querySelectorAll('a[href*="#"')).forEach(function(anchorLink) {
  var url = anchorLink.href;
  var targetId = url.split('#')[1];
  var targetTop;

  if (!anchorLink.classList.contains('no-scroll')) {
    anchorLink.addEventListener('click', function(e) {

      if (targetId) {
        e.preventDefault();

        if (targetId === "apply") {
          targetTop = page.scrollTop
          + document.getElementById(targetId).getBoundingClientRect().top;
        } else {
          targetTop = page.scrollTop
          + document.getElementById(targetId).getBoundingClientRect().top
          - NAVBAR_OFFSET;
        }

        smoothScrollTo(targetTop, 500);
      }
    });
  }
});

function scrollToFirstInvalidInput(form, submitButton, onSubmitAttempt,
  preventValidation) {

  var targetTop;

  var fields = form.elements;

  // prevent an infinate loop when click is synthesised in setTimeout below
  submitButton.removeEventListener("click", onSubmitAttempt);

  for (var i = 0; i < fields.length - 1; i++) {

    if ((typeof fields[i].checkValidity == "function")
      && !fields[i].checkValidity()) {
      targetTop = page.scrollTop
        + fields[i].getBoundingClientRect().top
        - (NAVBAR_OFFSET * 1.5); // extra height for the input label

      smoothScrollTo(targetTop, SCROLL_TIME);

      setTimeout(function() {
        // enable validation notification
        fields[i].removeEventListener("invalid", preventValidation);

        // trigger validation check
        submitButton.click();

        // re-suppress pre-scroll validation notification
        fields[i].addEventListener("invalid", preventValidation);

        submitButton.addEventListener("click", onSubmitAttempt);
      }, (SCROLL_TIME * 1.5)); // extra time to align nicely with the input

      break;
    }
  }
}
