window.addEventListener("DOMContentLoaded", function() {

  var stakeholderGroup = document.getElementById("stakeholder-group");
  var defaultStakeholderGroup = document.getElementById("default-stakeholder-group");

  stakeholderGroup.addEventListener("focus", function() {
    defaultStakeholderGroup.style.display = "none";
  });

  stakeholderGroup.addEventListener("blur", function() {
    defaultStakeholderGroup.style.display = "block";
  });

  var form = document.getElementById("join");
  var submitButton = document.getElementById("submit");

  function preventValidation(e) {
    e.preventDefault();
  }

  function onSubmitAttempt() {
    // Check if the form is valid. If not, proceed.
    // If reportValidity is not supported, proceed.
    if (!((typeof form.reportValidity == "function") && form.reportValidity()))
      // Check if individual inputs are valid.
      scrollToFirstInvalidInput(form, submitButton, onSubmitAttempt,
        preventValidation);
  }

  // suppress validation notifications, pre-submit/scroll
  [].slice.call(form.elements).forEach(function(field) {
    field.addEventListener("invalid", preventValidation);
  });

  submitButton.addEventListener("click", onSubmitAttempt);

  form.addEventListener("submit", function(event) {

    event.preventDefault();

    submitButton.disabled = true;
    document.getElementById("sending-message").hidden = null;

    var params = "";
    var fields = form.elements;

    // Construct query string
    for (var i = 0; i < fields.length - 1; i++)
      if (fields[i] && fields[i].name && fields[i].value)
        params += fields[i].name + "=" + encodeURIComponent(fields[i].value) + "&";
    params = params.slice(0, -1); // Remove last "&"

    var request = new XMLHttpRequest();
    request.open("POST", "/committee/apply/submit", true);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    request.addEventListener("readystatechange", function() {
      if (request.readyState == 4) {
        document.getElementById("sending-message").hidden = true;
        if (request.status == 200) {
          document.getElementById("success-message").hidden = null;
          document.getElementById("error-message").hidden = true;
        } else {
          document.getElementById("success-message").hidden = true;
          document.getElementById("error-message").hidden = null;
        }
      }
    });

    request.send(params);
  });
});
