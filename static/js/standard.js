addEventListener('load', function() {
  var targetEl = document.querySelector('.footnotes2');
  var dynamicBgEl = document.querySelector('.bg-dynamic');
  var footerEl = document.getElementById('footer');

  var placementContent = document.getElementById('placement');
  var distinctionContent = document.getElementById('distinction');
  var sizeContent = document.getElementById('size');
  var tabLinks =
    Array.prototype.slice.call(document.querySelectorAll('.tabs li a'));

  var tabContent = [placementContent, distinctionContent, sizeContent];

  var tabsMap = {
    placement: placementContent,
    distinction: distinctionContent,
    size: sizeContent
  };

  function switchTabContent(element) {
    tabContent.forEach(function(content) {

      if (!content.hasAttribute('hidden'))
        content.setAttribute('hidden', '');
    });

    element.removeAttribute('hidden');
  }

  tabLinks.forEach(function(a) {
    a.addEventListener('click', function(e) {
      e.preventDefault();

      switchTabContent(tabsMap[this.href.split('#')[1]]);

      tabLinks.forEach(function(a) {

        if (a.classList.contains('selected'))
          a.classList.remove('selected');
      });

      this.classList.add('selected');

      positionDynamicBackground();
    });
  });

  function positionDynamicBackground() {
    var targetYOffset = targetEl.getBoundingClientRect().top;
    var footerYOffset = footerEl.getBoundingClientRect().top;

    dynamicBgEl.style.height = (footerYOffset - targetYOffset) + 'px';

    dynamicBgEl.style.top = (pageYOffset + targetYOffset) + 'px';
  }

  positionDynamicBackground();

  addEventListener('resize', positionDynamicBackground);
});
